<?php
//headers
header('Access-Control-Allow-Origin: *');
header('Content-Type: application/json'); 
header('Access-Control-Allow-Methods:PUT');
header('Access-Control-Allow-Headers:Access-Control-Allow-Headers,Content-Type,Access-Control-Allow-Methods,Authorisation');
//initilizing  the api
include_once('../core/initilize.php');
//inistitating the post
$post=new Post($db);
//get the posted data
$data=json_decode(file_get_contents("php://input"));

$post->id=$data->id;
$post->title=$data->title;
$post->body=$data->body;
$post->author=$data->author;
$post->category_id=$data->category_id;
//create the post
if($post->update())
{
    echo json_encode(array('messgae'=>'Post updated'));

}
else {
    echo json_encode(array('messgae'=>'Post not updated'));
}
